# mastodon

This is (mostly?) documentation about the mastodon instance.  

## Setup Documents

* [Setup](setup.md) Setup on Gentoo
* [SSH Setup](ssh.md) Some information about setting up SSH.

## Misc Other Documents

* [Contributing](CONTRIBUTING.md)
* [Changelog](CHANGELOG)