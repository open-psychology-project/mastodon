Current Blocks and status of them

At this point we have had no need to block any accounts, or instances.  
This likely will change.

Blocking we hope to feel will be able to be done on the basis of whether 
or not the accounts or instances are affecting local users.  We wish to 
encourage local users to report accounts and explain why (if they feel
up to it) as well as using mute/block functions.

| User/Instance | Action               | Reason                        |
| ------------- | -------------------- | ----------------------------- |
| baraag.net    | Silence, Media Block | Child/Adolescent Porn imagery | 
| pawoo.net     | Silence, Media Block | Child/Adolescent Porn imagery |
| gameliberty.club (1) | Suspend | Free Speech Instance |
| sukebeneko.com (2) | Silence, Media Block | Child Pornography |
| hackers.town (3) | Suspened | Works with law enforcement |
| masto.pt (4) | Suspend | Supports hackers.town admin | 
| chaos.social (5) | Suspend | Tech Bros |

1. This has encouraged the harrasment of anyone who has reported any user on 
the instance.  They have a rule "can upload other's content" as well as
"don't post anything illegal".  Uploading any content without explicate 
permission (licensing would be considered such) is copyright infringement
in all cases, thus illegal.  "Boosting is the same as posting" thus they
have rules which are self-conflicting in a massive way...
2. Very small instance, with almost all toots from a single account.  
3. The admin admits to having working with FBI in the past, and continues to 
officially work with them "when needed".  Despite the fact that we believe 
that people should "act within the law" there is no trust in law enforcement 
to do so.
4. This is probably a massive mistake on my part, but I just have to do this.
Masto.pt moderator? (eloisa) feels that there is a "witch hunt" against 
hackers.town and this makes her "sad".  When I explained my understanding 
she told me that, "it's not like that," and told me to read a bunch of stuff
which would "say the truth."  None of it was any different than what I had
said in terms of facts, but it was different in terms of the interpretation
of them, and for the most part, I didn't even put my interpretation beyond 
this is what people are saying...  Took much of this to direct message...  
5. This is a bit nebulous, but since this instance is just myself, and one 
other person who is not active on here right now, I can go and explain this
chaos.social is a site which is focused on hackers (using the term correctly)
which in theory should not be a big deal because, well there's nothing wrong
with being a tech focused person.  The problem isn't that, it's the attitudes
which come out of the tech industry where the likes of Elon Musk and 
Richard Stallman end up getting defended because they are, "really smart."
The problem is that this means that people are defending people who are 
clearly highly problematic, on the basis of "merit," and this leads to a 
sense with those within the community that promoting your merit, is an 
important thing, and integrity is very low on the list of important character
traits.  This came out of a long discussion in private with one of the memebers
who has made claims of "expertise" about what data security and why they feel
that deleting public posts protects that data.  If they believe that they are
actually protecting that data, they are not aware of broadly publicly available
information about the degree of indexing that exists (end to end messages are
almost certainly indexed (probably at least 90%)) so the idea that you can 
protect publicly posted stuff from "indexing" by deleting it after 7 days is 
at best misguided.  What you *can* protect by doing that, is your "appearance 
of integrity," ie. it becomes more difficult to show that you are not
maintaining integruity if the post is no longer where it originally was.