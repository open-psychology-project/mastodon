This is mostly about what the intention of this site is and how users
will end up interacting with it.

First off, this site was mostly setup as a proof of concept as to being
able to set this up in a way that works, but also with the hope that 
either for the Open Psychology Project which has been pretty quite of late
or the project that we got talking with our friend about joining 
officially.

So, as of the writing of this (late evening 2019 February 13) the instance
is accepting and has been somewhat publicly stated we are accepting new
registrations.  

That said, as of writing this, a big part of why we are accepting new 
registrations is to be able to better test to see what might be needed to
fix the issues which currently exist with it.  

Which means, there are known issues which at this time haven't been fixed.

Assuming we do find a reasonable fix for these known issues, we will at 
thistime be running this as part of the Open Psychology Project which 
means that we will be working on the concepts that we have around that.

Essentially this came out of frustration around how the process of the 
autism assessment went for myself (Jigme Datse Yli-Rasku @JigmeDatse 
on the instance) and how at least the process which I went through was
intentionally closed.

We have (and believe there is good evidence across fields) come to believe
that trying to keep things secret, increases the likelyhood that when 
people find out anything about it, they will rather keep that secret, and
either use it as an exploit of the system themselves, or share it in quite
corners, that they trust that they'll not be found out about.  

This also applies to things like psychology and other social sciences.

So, the main (initial) focus was the development of resources and tools
which could be open, and people could use, and develop them as they saw
fit.  And an attempt to be able to do research based on the idea that 
the secrecy which is embeded in the current way the social sciences work
is actually harming them, and thus producing open research, with open data
and open access papers.  

So, this is open to *anyone* who shares these goals, and interests, no 
matter where you are with regards to them.  Expected topics include:

* Autism
* Personality Disorders
* Social Justice
* Plurality
* Eating Disorders
* Addiction
* Self care
* Self expression

There will probably be more topics too.  Feel free to join if any of this
sounds like you'd potentially fit in here.  Or even if you might not fit 
in, because you don't quite see where any of this applies to you.  