I'm hoping to keep this up to date, but it might not be fully up to date so feel 
free to "blame" if you see something that isn't as it should be.

This is an initial commit, just as we have started to more broadly open the 
instance up, well but I want to list the staff.

Admin:

* [lead](https://mastodon.openpsychology.net/web/accounts/1) - Main admin of the instance, currently Jigme Datse Yli-Rasku.  May change in the future if this gets handed over.

Moderators:

* [JigmeDatse](https://mastodon.openpsychology.net/web/accounts/212) - More personal account set as a moderator.  
* [Pluto](https://mastodon.openpsychology.net/web/accounts/233) - Setsuna Meiō personal friend who inspired me to set this up.
 
Helpers:

These are people who have contributed significantly, but are neither moderators 
nor admins

