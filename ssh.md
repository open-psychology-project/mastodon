OK, I need to document a bit about setting things up with regards to SSH.

The thing that I always end up forgetting to do before I run into problems with
it, is fix the issues that can happen with session timeouts.  

On my client side in `/etc/ssh/ssh_config` I have:

```
Host *
  ServerAliveInterval 120
  ServerAliveCountMax 720
```

to make sure that the client side doesn't timeout.  This is a really long
timeout setting, and probably is excessive, but it also means that the 
session won't unexpectedly timeout which is really important when working with
Gentoo, because sometimes you can be working on an update, or something
for 8 or more hours.  

Then, on the server side, in `/etc/sshd_config` I have:

```
ClientAliveInterval 120
ClientAliveCountMax 720
```

Again, very long, it is actually 24 hours.  So, it's probably longer than you
really need it.  Though in the past, I've had stuff like a world update, or
kernel build take up to 72 hours.  Though it doesn't seem to be that often 
now that I am even working on the same thing for 72 hours.  