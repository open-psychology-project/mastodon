# Mastodon Setup

Some documentation about setting up Mastodon, this is being written as we 
are setting this up.

## System Setup

This is getting the system ready to be able to run Mastodon.

```
emerge --sync
emerge eix
emerge -uDaNt @world
```

This gets the system up to a base state that I am familiar with.

```
emerge --ask nodejs yarn
```

This is a deviation from the 
[official mastodon documentation](https://docs.joinmastodon.org/administration/installation/) 
which has a call through curl to a bash script, which is relying on 
the presence of `apt-get` which we don't have.

It appears that when we tried to install, we were running into a problem that
looks like it was an out of memory error.  We may have been able to get around
it by increasing the amount of swap space, but instead increased to the next
largest linode instance.  

## Preparing to install Mastodon

This is getting the system ready to install any of the parts that are available
and expected on the system.

```
emerge --ask imagemagick ffmpeg libpqxx dev-vcs/git dev-libs/protobuf \
protobuf-c libyaml nginx dev-db/redis postgresql certbot libidn \
dev-libs/icu dev-libs/jemalloc
```

This is based on what the official documentation says for the Ubuntu
install.  There are some use changes I did as well:

```
USE="corefonts jpeg png svg truetype encode jpeg2k mp3 openh264 theora"
USE="$USE vorbis x264 x265"
```

## Setting up environment for Mastodon

At this point, I believe we are at a point that most of what needs to be done
will not be much, if any slower than running at the home machine that I started
to work on, as everything else will not be using (unless I'm mistaken), coming
from the Gentoo portage tree (or any overlay).

```
groupadd mastodon
useradd -c "Mastodon instance user" -g mastodon -G users mastodon
```

This I think could be shortend to:

```
useradd -c "Mastodon instance user" -U -G users mastodon
```

with the same results.  

Then we need to get into that user account:

```
su - mastodon
```

And setup rbenv:

```
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
cd ~/.rbenv && src/configure && make -C src
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
exec bash
git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
```

This sets things up to be able to install Ruby.

At this point I take another deviation from the installation documentation 
and this could be a mistake on my part, but I ran into a problem I wasn't sure
how to resolve with the default instrcutions, and my solution is just different
from what they have done.

```
RUBY_CONFIGURE_OPTS=--with-jemalloc rbenv install 2.6.0
rbenv global 2.6.0
```

Then we update rubygems:

```
gem update --system
```

And install a newer version of bundler:

```
gem install bundler
```

Which last time we didn't do because it conflicts with the version that is
already installed.  But let's try this time with doing this.

We now return to the root account:

```
exit
```

## Setting up PostgreSQL

I'm not sure about using the "pgTune" which should optimize the settings, but
since it is not based on the system itself, I think I'm going to end up 
skipping that, and look at that if I have to in the future.

### Configuring (starting etc) PostgreSQL

At this point, PostgreSQL is not up and running, and needs to be setup.  
This could have been done earlier, but it wasn't needed until now:

```
emerge --config dev-db/postgresql:
rc-service postgresql-11 start
rc-update add postgresql-11 default
```

It now should start at system startup.  

### Creating PostgreSQL user

```
sudo -u postgres psql
```

Then at the prompt you type:

```
CREATE USER mastodon CREATEDB;
\q
```

This creates the user that is needed.  I would recommend having some aditional
"paranoia" settings, but I'm not quite sure how they would work with how the 
setup is later down the line.


## Setting up Mastodon

We are now looking at specifics to Mastodon.

Get back in that mastodon user shell:

```
su - mastodon
```

### Checking out the code

We need to get the code onto the local server.

```
git clone https://github.com/tootsuite/mastodon.git live && cd live
git checkout $(git tag -l | grep -v 'rc[0-9]*$' | sort -V | tail -n 1)
```

### Installing last dependencies

This is where the previous before I did changes ran into the first obvious 
problems, it complained that it couldn't do these steps.  

```
bundle install \
  -j$(getconf _NPROCESSORS_ONLN) \
  --deployment --without development test
yarn install --pure-lockfile
```

This should get us to the point that we can start configuring the server

There are a few things that happen here, it seems that for whatever reason 
when I get to this point, I have the terminal session lock, and need to get
out of it.  And second, I keep forgetting that to configure the server
I need to have the redis service running.

### Setting up Redis

This is pretty simple really:

```
rc-service redis start
rc-service redis-sentinel start
rc-update add redis default
rc-update add redis-sentinel default
```

I am not sure I need the `redis-sentinal` running or not, but I don't think it
is a bad thing.  

### Generating a configuration

This point you generate the configuration.

This you need to be in the right place, with the right user so if you're not
already in `~mastodon/live` as the user `mastodon` you need to get back to 
being there.  I have had to do this a couple of times before going through
this process:

```
su - mastodon
cd live
```

Then you need to execute:

```
RAILS_ENV=production bundle exec rake mastodon:setup
```

You need some information for this:

* Domain Name
* Single User Mode (usually no)
* PostgreSQL info:
   * host
   * port
   * database
   * user
   * user password
* Redis info:
   * host
   * port
   * password
* Cloud storage (I am saying N)
* email setup:
   * SMTP server
   * SMTP port
   * SMTP username
   * SMTP password
   * SMTP authentication
   * SMTP OpenSSL verify mode
   * Email From

Then you can save the configuration, the email offers an opportunity to send a 
test email, and this tests the configuration, which is a good idea.  

### NGINX setup

This is a little different than the official documentation says, but not a
whole lot.  Gentoo doesn't create the same structure as Ubuntu, so you have
to do something a little different.

I create a structure which is similar to Ubunutu:

```
mkdir /etc/nginx/sites-available /etc/nginx/sites-enabled
```

That creates the directory structure.  Then I need to call the 
`/etc/nginx/sites-enabled` files by adding `include /etc/nginx/sites-enabled/*;`
within the http section of `/etc/nginx/nginx.conf`.  

This allows that the "expected" behaviour that people have, can actually be
at least mostly handled with gentoo.  

Once that is done, you can follow the instructions in the officia docmentation:

```
cp /home/mastodon/live/dist/nginx.conf /etc/nginx/sites-available/mastodon
ln -s /etc/nginx/sites-available/mastodon /etc/nginx/sites-enabled/mastodon
```

Then edit, `/etc/nginx/sites-available/mastodon` replacing `example.com` with
your actual domain name.

Then you restart nginx:

```
rc-service nginx restart
```

There are a few things that I have done which have reduced the errors, but
have not resulted in fixing things, one probably is most important.

```
large_client_header_buffers 4 16k;
```

This helps to deal with "client sent too large headers" that I was getting, but
it looks like maybe putting something on the "upstream" portions such as:

```
    proxy_buffer_size          128k;
    proxy_buffers              4 256k;
    proxy_busy_buffers_size    256k;
```

may be helpful, as I'm not seeing errors to see what the upstream is doing
or getting errors indicating the upstream is doing something wrong.

Another possible fix was increasing the timeouts for the two upstream services.

I will once I get it working, put the updated version of the nginx 
configuration.

### Certificate

We use Let's Encrypt, because it seems to work.  

```
certbot certonly --webroot -d example.com -w /home/mastodon/live/public/
```


Again replacing `example.com` with your actual domain name.

Then in `/etc/nginx/sites-available/mastodon` you uncomment, the 
`ssl_certificate` and `ssl_certificate_key` and make sure you have
the right location (changing `example.com` to your domain again).

Then you need to restart nginx again:

```
rc-service nginx restart
```

### OpenRC Init Scripts

We found https://github.com/tootsuite/documentation/blob/master/Running-Mastodon/Alternatives.md
which contained OpenRC init scripts, which didn't actually work but only needed
some slight modifications.  

We have [OpenRC directory](openrc) files here.  